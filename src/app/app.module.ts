import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider, LoginOpt} from "angularx-social-login";

const googleLoginOptions: LoginOpt = {  
  scope: ["https://www.googleapis.com/auth/yt-analytics.readonly"].join(" ")
};
let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("31424254074-m9adfjvo6cg2cvntiujok6hq2qjj8os6.apps.googleusercontent.com", googleLoginOptions)
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("2219294408323240")
  },
]);
 
export function provideConfig() {
  return config;
}

import {
    GoogleApiModule, 
    GoogleApiService, 
    GoogleAuthService, 
    NgGapiClientConfig, 
    NG_GAPI_CONFIG,
    GoogleApiConfig
} from "ng-gapi";
import { YtTestComponent } from './yt-test/yt-test.component';

let gapiClientConfig: NgGapiClientConfig = {
    client_id: "31424254074-m9adfjvo6cg2cvntiujok6hq2qjj8os6.apps.googleusercontent.com",
    discoveryDocs: ["https://youtubeanalytics.googleapis.com/$discovery/rest?version=v2"],
    scope: [
        "https://www.googleapis.com/auth/yt-analytics.readonly",
    ].join(" ")
};

@NgModule({
  declarations: [
    AppComponent,
    YtTestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SocialLoginModule,
    GoogleApiModule.forRoot({
            provide: NG_GAPI_CONFIG,
            useValue: gapiClientConfig
          })
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
