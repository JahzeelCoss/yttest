import { Injectable, NgZone } from '@angular/core';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import { Observable, forkJoin } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ReportService {

  	user: SocialUser;  
	observableUser: BehaviorSubject<SocialUser>;
	googleLoggedIn: BehaviorSubject<boolean> = new  BehaviorSubject<boolean>(false);
	facebookLoggedIn: BehaviorSubject<boolean> = new  BehaviorSubject<boolean>(false);;
	private authLoggedIn: boolean;
	public static readonly GOOGLE_ACCESS_TOKEN: string = "Bronze_googleUserToken";
	public static readonly FACEBOOK_ACCESS_TOKEN: string = "Bronze_facebookUserToken";
	public completeReport: any;
  	public reports: any[] = [];

	YTreportApiUrl: string = "https://youtubeanalytics.googleapis.com/v2/reports?";


	constructor(private authService: AuthService, private http: HttpClient,private ngZone: NgZone) 
	{ 		
		if(localStorage.getItem(ReportService.GOOGLE_ACCESS_TOKEN) != null)
		{
			// console.log("google token");
			this.googleLoggedIn.next(true);
		}
		if(localStorage.getItem(ReportService.FACEBOOK_ACCESS_TOKEN) != null)
		{
			// console.log("facebook token");
			this.facebookLoggedIn.next(true);
		}

		this.authService.authState.subscribe((user) => {	    	
	    	this.user = user;	    		    	
	    	this.observableUser.next(this.user); 
	    	this.authLoggedIn = (user != null);	
	    	if(this.authLoggedIn)
	    	{
	    		if(this.user.provider === "GOOGLE")
	    		{
	    			this.googleLoggedIn.next(true);
	    			localStorage.setItem(ReportService.GOOGLE_ACCESS_TOKEN, this.user.authToken);
	    		}
	    		if(this.user.provider === "FACEBOOK")
	    		{
	    			this.facebookLoggedIn.next(true);
	    			localStorage.setItem(ReportService.FACEBOOK_ACCESS_TOKEN, this.user.authToken);
	    		}
	    	}
	    	// console.log("login in alert callback");  
		});	
		this.observableUser = new BehaviorSubject<SocialUser>(this.user);		
	}

	login(providerName)
	{
		if(this.authLoggedIn)
		{

		}
		else
		{
			switch (providerName) {
				case "facebook":
					this.signInWithFB();
					break;
				case "google":
					this.signInWithGoogle();
					break;			
				default:							
					break;
			}
		}
		
	}

	signInWithFB()
	{
	    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);	   
	}

	signInWithGoogle()
	{
		this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);	   
	}

	logout()
	{			
		this.authService.signOut();		
		this.authLoggedIn = false;
		// localStorage.clear();
		// localStorage.removeItem("key");
	}

	public getGoogleToken(): string {
        let token: string = localStorage.getItem(ReportService.GOOGLE_ACCESS_TOKEN);
        if (!token) {
            throw new Error("no google token set , authentication required");
        }
        return localStorage.getItem(ReportService.GOOGLE_ACCESS_TOKEN);
    }

    public getFacebookToken(): string {
        let token: string = localStorage.getItem(ReportService.FACEBOOK_ACCESS_TOKEN);
        if (!token) {
            throw new Error("no facebook token set , authentication required");
        }
        return localStorage.getItem(ReportService.FACEBOOK_ACCESS_TOKEN);
    }

  	private extractData(res: Response) {
  		let body = res;
  		return body || { };
	}
	
	exec2(): Observable<any> {
		const headers = new HttpHeaders({
			'Accept':  'application/json',
    		'Authorization': 'Bearer '+this.getGoogleToken()
		});
	  return this.http.get('https://youtubeanalytics.googleapis.com/v2/reports?dimensions=video&endDate=2018-05-01&ids=channel%3D%3DMINE&maxResults=10&metrics=estimatedMinutesWatched%2Cviews%2Clikes%2CsubscribersGained&sort=-estimatedMinutesWatched&startDate=2017-01-01',{headers}).pipe(
	    map(this.extractData));
	}

	videoInfo(videoId): Observable<any> {
		const headers = new HttpHeaders({
			'Accept':  'application/json',
    		'Authorization': 'Bearer '+this.getGoogleToken()
		});
	  return this.http.get(this.YTreportApiUrl+'endDate=2018-05-01&filters=video%3D%3D'+videoId+'&ids=channel%3D%3DMINE&metrics=views%2Clikes%2Cdislikes&startDate=2017-01-05',{headers}).pipe(
	    map(this.extractData));
	}

	getFormattedCurrentDate()
	{
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; 
		var yyyy = today.getFullYear();
		var day = ""+dd;
		var month = ""+mm;		
		if(dd<10) 
		{
		    day = '0'+dd;
		} 
		if(mm<10) 
		{
		    month = '0'+mm;
		} 

		var todayString = yyyy+'-'+month+'-'+day;
		// console.log(todayString);
		return todayString;
	}

	getYoutubeReport(videoId): Observable<any[]> 
	{	
		return new Observable((observer) => {
			this.getYoutubeData(videoId).subscribe((data)=>{
	       		// console.log(data);
	       		this.reports.push(data);
	       		return this.processYoutubeData(data);	       		
	     	});
		});
		// return this.processYoutubeData(this.getYoutubeData(videoId));
	}

	//NzQ1RKv9sX0 test
	getYoutubeData(videoId): Observable<any[]> {
		const headers = new HttpHeaders({
			'Accept':  'application/json',
    		'Authorization': 'Bearer '+this.getGoogleToken()
		});		
		const today = this.getFormattedCurrentDate();
		const oldDate = "2000-01-01";

	    let viewsResponse = this.http.get(this.YTreportApiUrl+"dimensions=liveOrOnDemand&endDate="+today+"&filters=video%3D%3D"+videoId+"&ids=channel%3D%3DMINE&metrics=views%2CestimatedMinutesWatched&startDate="+oldDate,{headers});
	    let devicesResponse = this.http.get(this.YTreportApiUrl+"dimensions=deviceType&endDate="+today+"&filters=video%3D%3D"+videoId+"&ids=channel%3D%3DMINE&metrics=views&startDate="+oldDate,{headers});
	    let provincesResponse = this.http.get(this.YTreportApiUrl+"dimensions=province&endDate="+today+"&filters=video%3D%3D"+videoId+"%3Bcountry%3D%3DUS&ids=channel%3D%3DMINE&maxResults=5&metrics=views&sort=-views&startDate="+oldDate,{headers});
	    let demographicsResponse = this.http.get(this.YTreportApiUrl+"dimensions=ageGroup%2Cgender&endDate="+today+"&filters=video%3D%3D"+videoId+"&ids=channel%3D%3DMINE&metrics=viewerPercentage&sort=-viewerPercentage&startDate="+oldDate,{headers});
	    return forkJoin([viewsResponse, devicesResponse, provincesResponse, demographicsResponse]);
	}


	processYoutubeData(youtubeData)
	{
		let viewsResponse = youtubeData[0];
		let devicesResponse = youtubeData[1];
		let provincesResponse = youtubeData[2];
		let demographicsResponse = youtubeData[3];


		// VIEWS INFO
		let recordedViews = 0;
		let liveViews = 0;
		let onDemandViews = 0;
		let estimatedMinutesWatched = 0;

		// console.log(viewsResponse.rows[0]);
		// console.log(viewsResponse.rows[0][0]);
		// console.log( Array.isArray( viewsResponse.rows[0]));
		// 0: (live or Ondemand) 1: views 2:estimatedMinutesWatched
		// for (var i = 0; i < viewsResponse.rows[0].length; i++) 
		// {
		// 	if(viewsResponse.rows[0][i] === "LIVE")
		// 	{
		// 		recordedViews += viewsResponse.rows[0][i+1];
		// 		liveViews = viewsResponse[0][i+1];
		// 		estimatedMinutesWatched += viewsResponse[0][i+1];
		// 		console.log("live: "+liveViews);
		// 	}
		// 	if(viewsResponse.rows[0][i] === "ON_DEMAND")
		// 	{
		// 		recordedViews += viewsResponse.rows[0][i+1];
		// 		onDemandViews = viewsResponse.rows[0][i+1];
		// 		console.log("demand: "+onDemandViews);
		// 	}
		// 	console.log("nada");
		// }	
		// console.log(viewsResponse.rows.length);
		for (var i = 0; i < viewsResponse.rows.length; i++) 
		{		
			for (var j = 0; j < viewsResponse.rows[i].length; j++) 
			{
				if(viewsResponse.rows[i][j] === "LIVE")
				{
					recordedViews += viewsResponse.rows[i][j+1];
					liveViews = viewsResponse[i][j+1];
					estimatedMinutesWatched += viewsResponse[i][j+1];
					// console.log("live: "+liveViews);
				}
				if(viewsResponse.rows[i][j] === "ON_DEMAND")
				{
					recordedViews += viewsResponse.rows[i][j+1];
					onDemandViews = viewsResponse.rows[i][j+1];
					// console.log("demand: "+onDemandViews);
				}
			}		
		}

		//DEVICES INFO
		let desktopViews = 0;
		let tabletViews = 0;
		let mobileViews = 0;
		let TVViews = 0;

		for(var i = 0; i < devicesResponse.rows[0].length; i++)
		{
			for (var j = 0; j < devicesResponse.rows[i].length; j++) 
			{
				switch (devicesResponse.rows[i][j]) {
					case "DESKTOP":
						desktopViews = devicesResponse.rows[i][j+1];
						break;
					case "MOBILE":
						mobileViews = devicesResponse.rows[i][j+1];
						break;
					case "TABLET":
						tabletViews = devicesResponse.rows[i][j+1];
						break;
					case "TV":
						TVViews = devicesResponse.rows[i][j+1];
						break;
					default:
						break;
				}				
			}	  			
		}
		// console.log("desk "+ desktopViews);
		// console.log("tablet "+ tabletViews);
		// console.log("mobile "+ mobileViews);
		// console.log("desk "+ TVViews);		

		//LOCATIONS INFO
		let provinces = provincesResponse.rows;
		//DEMOG INfo
		let demographicInfo = demographicsResponse.rows;

		//REPORT
		let reportJson = 
		{
			'recordedViews': recordedViews,
			'liveViewers': liveViews,
			'onDemandViewers': onDemandViews,
			'devices': {
				'desktop': desktopViews,
				'mobile': mobileViews,
				'tablet': tabletViews,
				'tv': TVViews
			},
			'provinces': provinces,
			'demographicInfo': demographicInfo
		}

		return reportJson;		
	}

	
}
