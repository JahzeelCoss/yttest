import { Injectable, NgZone } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import {
    GoogleApiModule, 
    GoogleApiService, 
    GoogleAuthService, 
    NgGapiClientConfig, 
    NG_GAPI_CONFIG,
    GoogleApiConfig,
} from "ng-gapi";
import GoogleUser = gapi.auth2.GoogleUser;
declare var gapi: any;
// const 	headers = new HttpHeaders();
// const httpOptions = {
//   headers: new HttpHeaders({
//     'Accept':  'application/json',
//     'Authorization': 'Bearer ya29.GlyuBgv3DuMspgEbtyRv0HkW40FDVMJwcVn0xep_brViSMXSxLVBvNR02v3-TRkGP0RxlzRR45J2obxRDzmngvUH5L9ZO9ldtGFsoVU0UnNqLJCcnjekyjeImM7SmA'
//   })
// };

@Injectable({
  providedIn: 'root'
})
export class YoutubeService {
	public static readonly SESSION_STORAGE_KEY: string = "accessToken";
    private user: GoogleUser = undefined; 
    YTreportApiUrl: string = "https://youtubeanalytics.googleapis.com/v2/reports?";

  	constructor(gapiService: GoogleApiService, private googleAuthService: GoogleAuthService, private http: HttpClient,private ngZone: NgZone) {
  		gapiService.onLoad().subscribe(()=> {
          	// console.log("usar api");
          	
        });
   	}

   	public setUser(user: GoogleUser): void {
        this.user = user;
    }

    public getCurrentUser(): GoogleUser {
        return this.user;
    }

    public getToken(): string {
        let token: string = sessionStorage.getItem(YoutubeService.SESSION_STORAGE_KEY);
        if (!token) {
            throw new Error("no token set , authentication required");
        }
        return sessionStorage.getItem(YoutubeService.SESSION_STORAGE_KEY);
    }

   	check()
   	{
   		this.googleAuthService.getAuth().subscribe((auth) => {
            auth.signIn().then(res => this.signInSuccessHandler(res), err => console.log("unsucc"));
        });
   	}

   	private signInSuccessHandler(res: GoogleUser) {
        this.ngZone.run(() => {
            this.user = res;
            sessionStorage.setItem(
                YoutubeService.SESSION_STORAGE_KEY, res.getAuthResponse().access_token
            );
            // this.defineHeaders();
            // console.log("token: "+this.getToken());
        });
    }

  //   defineHeaders(){
  //   	console.log("defining headers"+this.getToken());    	
  //   	headers.append('Accept', 'application/json');
		// headers.append('Authorization', 'Bearer '+this.getToken());
		// console.log(headers);
  //   }

 //   	checkAuth() {
	//     console.log("checkingAuth");
	//     gapi.auth.authorize({
	//       client_id: '31424254074-m9adfjvo6cg2cvntiujok6hq2qjj8os6.apps.googleusercontent.com',
	//       scope: [
	// 	    'https://www.googleapis.com/auth/yt-analytics.readonly',
	// 	    'https://www.googleapis.com/auth/youtube.readonly'
	// 	  ],
	//       immediate: true
	//     }, this.handleAuthResult);
	// }

	// Handle the result of a gapi.auth.authorize() call.
 	handleAuthResult(authResult) {
	    console.log("handling result");
	    if (authResult) {
	      // Authorization was successful. Hide authorization prompts and show
	      // content that should be visible after authorization succeeds.
	      // $('.pre-auth').hide();
	      // $('.post-auth').show();

	      // loadAPIClientInterfaces();
	      console.log("auth succ");
	    } else {
	      // Authorization was unsuccessful. Show content related to prompting for
	      // authorization and hide content that should be visible if authorization
	      // succeeds.
	      // $('.post-auth').hide();
	      // $('.pre-auth').show();

	      // Make the #login-link clickable. Attempt a non-immediate OAuth 2.0
	      // client flow. The current function is called when that flow completes.
	      // $('#login-link').click(function() {
	      //   gapi.auth.authorize({
	      //     client_id: OAUTH2_CLIENT_ID,
	      //     scope: OAUTH2_SCOPES,
	      //     immediate: false
	      //   }, handleAuthResult);
	      // });
	      console.log("otra cosa");
	    }
	  }

	execute() {

	    return gapi.client.youtubeAnalytics.reports.query({
	      "ids": "channel==MINE",
	      "startDate": "2017-01-01",
	      "endDate": "2017-12-31",
	      "metrics": "views,estimatedMinutesWatched,averageViewDuration,averageViewPercentage,subscribersGained",
	      "dimensions": "day",
	      "sort": "day"
	    })
	        .then(function(response) {
	                // Handle the results here (response.result has the parsed body).
	                console.log("Response", response);
	              },
	              function(err) { console.error("Execute error", err); });
  	}  	

  	// getVideoInfo(videoId)
  	// {

	  //   return gapi.client.youtubeAnalytics.reports.query({
	  //     "ids": "channel==MINE",
	  //     "startDate": "2015-01-01",
	  //     "endDate": "2018-02-18",
	  //     "metrics": "views,subscribersGained,likes,dislikes",
	  //     "dimensions": "video",
	  //     "filters":
	  //   })
	  //       .then(function(response) {
	  //               // Handle the results here (response.result has the parsed body).
	  //               console.log("Response", response);
	  //             },
	  //             function(err) { console.error("Execute error", err); });
  	// }


  	private extractData(res: Response) {
  		let body = res;
  		return body || { };
	}
	
	exec2(): Observable<any> {
		const headers = new HttpHeaders({
			'Accept':  'application/json',
    		'Authorization': 'Bearer '+this.getToken()
		});
	  return this.http.get('https://youtubeanalytics.googleapis.com/v2/reports?dimensions=video&endDate=2018-05-01&ids=channel%3D%3DMINE&maxResults=10&metrics=estimatedMinutesWatched%2Cviews%2Clikes%2CsubscribersGained&sort=-estimatedMinutesWatched&startDate=2017-01-01',{headers}).pipe(
	    map(this.extractData));
	}

	videoInfo(videoId): Observable<any> {
		const headers = new HttpHeaders({
			'Accept':  'application/json',
    		'Authorization': 'Bearer '+this.getToken()
		});
	  return this.http.get(this.YTreportApiUrl+'endDate=2018-05-01&filters=video%3D%3D'+videoId+'&ids=channel%3D%3DMINE&metrics=views%2Clikes%2Cdislikes&startDate=2017-01-05',{headers}).pipe(
	    map(this.extractData));
	}

	// videoInfoDay()
	// {
	// 	const headers = new HttpHeaders({
	// 		'Accept':  'application/json',
 //    		'Authorization': 'Bearer '+this.getToken()
	// 	});
	//   return this.http.get(this.YTreportApiUrl+'endDate=2018-05-01&filters=video%3D%3D'+videoId+'&ids=channel%3D%3DMINE&metrics=views%2Clikes%2Cdislikes&startDate=2017-01-05',{headers}).pipe(
	//     map(this.extractData));
	// }

}

// views LIVE o ON_DEMAND
//https://youtubeanalytics.googleapis.com/v2/reports?dimensions=liveOrOnDemand&endDate=2018-05-01&filters=video%3D%3DNzQ1RKv9sX0&ids=channel%3D%3DMINE&metrics=views&startDate=2015-01-01 HTTP/1.1

// obtener views del dia de publicacion
// https://youtubeanalytics.googleapis.com/v2/reports?dimensions=day&endDate=2018-05-01&filters=video%3D%3DNzQ1RKv9sX0&ids=channel%3D%3DMINE&maxResults=1&metrics=views&startDate=2015-01-01

//obterner por provincia de us
// https://youtubeanalytics.googleapis.com/v2/reports?dimensions=province&endDate=2018-05-01&filters=video%3D%3DGrUq7_jQJrc%3Bcountry%3D%3DUS&ids=channel%3D%3DMINE&maxResults=5&metrics=views&sort=-views&startDate=2014-01-05
// https://youtubeanalytics.googleapis.com/v2/reports?dimensions=province&endDate=2014-06-30&filters=country%3D%3DUS&ids=channel%3D%3DMINE&maxResults=5&metrics=views&sort=province&startDate=2014-05-01

//obtener por pais
// https://youtubeanalytics.googleapis.com/v2/reports?dimensions=country&endDate=2018-05-01&ids=channel%3D%3DMINE&maxResults=10&metrics=views&sort=-views&startDate=2017-01-04 HTTP/1.1


//por dispositivo
// https://youtubeanalytics.googleapis.com/v2/reports?dimensions=deviceType&endDate=2018-05-01&filters=video%3D%3DNzQ1RKv9sX0&ids=channel%3D%3DMINE&metrics=views&startDate=2014-01-01 HTTP/1.1
// DESKTOP
// GAME_CONSOLE
// MOBILE
// TABLET
// TV
// UNKNOWN_PLATFORM
