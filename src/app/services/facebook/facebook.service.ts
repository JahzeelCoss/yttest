import { Injectable } from '@angular/core';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider,   } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import { Observable, of } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class FacebookService {

  user: SocialUser;  
	private loggedIn: boolean;
	observableUser: BehaviorSubject<SocialUser>;
	constructor(private authService: AuthService){	
		this.authService.authState.subscribe((user) => {	    	
	    	this.user = user;	    		    	    		
	    	this.observableUser.next(this.user);
	    	this.loggedIn = (user != null);	    
		});	
		this.observableUser = new BehaviorSubject<SocialUser>(this.user);	
	}

	// login(providerName)
	// {
	// 	switch (providerName) {
	// 		case "facebook":
	// 			this.signInWithFB();
	// 			break;
			
	// 		default:	
							
	// 			break;
	// 	}
	// }

	logout()
	{	
		this.authService.signOut();		
		localStorage.clear();
		this.loggedIn=false;
	}

	signInWithFB(){
	    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);	   
	}
	
}
