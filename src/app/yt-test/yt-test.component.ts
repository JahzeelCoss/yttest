import { Component, OnInit } from '@angular/core';
import { YoutubeService } from '../services/youtube.service';
import { ReportService } from '../services/reports/report.service';
import { SocialUser } from "angularx-social-login";
import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'app-yt-test',
  templateUrl: './yt-test.component.html',
  styleUrls: ['./yt-test.component.css']
})
export class YtTestComponent implements OnInit {

  constructor(private reportService: ReportService) { }
  // username: string;

  // user: SocialUser; 
  // private loggedIn: boolean;
  // private userSubscription: Subscription; 
  googlePermission: boolean;
  facebookPermission: boolean;
  dateNow: string;

  private providersTokenSubscription: Subscription;

  ngOnInit() {
    // this.userSubscription = this.reportService.observableUser.subscribe(item => {
    //   this.user= item;
    // });
    this.providersTokenSubscription = this.reportService.googleLoggedIn.subscribe(item => {
      this.googlePermission = item;
    });
    this.providersTokenSubscription = this.reportService.facebookLoggedIn.subscribe(item => {
      this.facebookPermission = item;
    });
    this.dateNow = this.reportService.getFormattedCurrentDate();    
  }

  logInWithGoogle()
  {
    this.reportService.login("google");
  }

  //video info
  getVideoInfo(videoId)
  {
    console.log("video: "+videoId); 
    this.reportService.getYoutubeReport(videoId).subscribe((data: {})=>{      
      console.log(data);
    });    
  }

  signInWithFB(): void {
    this.reportService.login("facebook");
  }  

  todayTest()
  {
    this.reportService.getFormattedCurrentDate();
  }
 

}
