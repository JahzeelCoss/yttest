export class Report {
	public total_views: number;
	public live_views: number;
	public minutes_watched: number;
	public devices_views: {
		desktop: number;
		mobile: number;
		tablet: number;
		tv: number;
		others: number;
	}
	public provinces:{
		
	}
}
